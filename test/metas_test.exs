defmodule MalApi.MetasTest do
  use MalApi.DataCase

  alias MalApi.Metas
  alias MalApi.Metas.Producer

  @create_attrs %{mal_id: 42, name: "some name"}
  @update_attrs %{mal_id: 43, name: "some updated name"}
  @invalid_attrs %{mal_id: nil, name: nil}

  def fixture(:producer, attrs \\ @create_attrs) do
    {:ok, producer} = Metas.create_producer(attrs)
    producer
  end

  test "list_producer/1 returns all producer" do
    producer = fixture(:producer)
    assert Metas.list_producer() == [producer]
  end

  test "get_producer! returns the producer with given id" do
    producer = fixture(:producer)
    assert Metas.get_producer!(producer.id) == producer
  end

  test "create_producer/1 with valid data creates a producer" do
    assert {:ok, %Producer{} = producer} = Metas.create_producer(@create_attrs)
    assert producer.mal_id == 42
    assert producer.name == "some name"
  end

  test "create_producer/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Metas.create_producer(@invalid_attrs)
  end

  test "update_producer/2 with valid data updates the producer" do
    producer = fixture(:producer)
    assert {:ok, producer} = Metas.update_producer(producer, @update_attrs)
    assert %Producer{} = producer
    assert producer.mal_id == 43
    assert producer.name == "some updated name"
  end

  test "update_producer/2 with invalid data returns error changeset" do
    producer = fixture(:producer)
    assert {:error, %Ecto.Changeset{}} = Metas.update_producer(producer, @invalid_attrs)
    assert producer == Metas.get_producer!(producer.id)
  end

  test "delete_producer/1 deletes the producer" do
    producer = fixture(:producer)
    assert {:ok, %Producer{}} = Metas.delete_producer(producer)
    assert_raise Ecto.NoResultsError, fn -> Metas.get_producer!(producer.id) end
  end

  test "change_producer/1 returns a producer changeset" do
    producer = fixture(:producer)
    assert %Ecto.Changeset{} = Metas.change_producer(producer)
  end
end
