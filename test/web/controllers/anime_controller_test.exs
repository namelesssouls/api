defmodule MalApi.Web.AnimeControllerTest do
  use MalApi.Web.ConnCase

  alias MalApi.Animes
  alias MalApi.Animes.Anime

  @create_attrs %{additional: "some additional", airing_end: 42, airing_start: 42, background: "some background", broadcast: "some broadcast", episode_duration: "some episode_duration", episodes: 42, favorites: 42, image: "some image", link_canonical: "some link_canonical", mal_id: 42, members: 42, popularity: 42, premiered: "some premiered", ranked: 42, rating: "some rating", score: 42, score_members: 42, source: "some source", status: "some status", synopsis: "some synopsis", title_english: "some title_english", title_japanase: "some title_japanase", title_romaji: "some title_romaji", title_synonyms: "some title_synonyms", type: "some type"}
  @update_attrs %{additional: "some updated additional", airing_end: 43, airing_start: 43, background: "some updated background", broadcast: "some updated broadcast", episode_duration: "some updated episode_duration", episodes: 43, favorites: 43, image: "some updated image", link_canonical: "some updated link_canonical", mal_id: 43, members: 43, popularity: 43, premiered: "some updated premiered", ranked: 43, rating: "some updated rating", score: 43, score_members: 43, source: "some updated source", status: "some updated status", synopsis: "some updated synopsis", title_english: "some updated title_english", title_japanase: "some updated title_japanase", title_romaji: "some updated title_romaji", title_synonyms: "some updated title_synonyms", type: "some updated type"}
  @invalid_attrs %{additional: nil, airing_end: nil, airing_start: nil, background: nil, broadcast: nil, episode_duration: nil, episodes: nil, favorites: nil, image: nil, link_canonical: nil, mal_id: nil, members: nil, popularity: nil, premiered: nil, ranked: nil, rating: nil, score: nil, score_members: nil, source: nil, status: nil, synopsis: nil, title_english: nil, title_japanase: nil, title_romaji: nil, title_synonyms: nil, type: nil}

  def fixture(:anime) do
    {:ok, anime} = Animes.create_anime(@create_attrs)
    anime
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, anime_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "creates anime and renders anime when data is valid", %{conn: conn} do
    conn = post conn, anime_path(conn, :create), anime: @create_attrs
    assert %{"id" => id} = json_response(conn, 201)["data"]

    conn = get conn, anime_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "additional" => "some additional",
      "airing_end" => 42,
      "airing_start" => 42,
      "background" => "some background",
      "broadcast" => "some broadcast",
      "episode_duration" => "some episode_duration",
      "episodes" => 42,
      "favorites" => 42,
      "image" => "some image",
      "link_canonical" => "some link_canonical",
      "mal_id" => 42,
      "members" => 42,
      "popularity" => 42,
      "premiered" => "some premiered",
      "ranked" => 42,
      "rating" => "some rating",
      "score" => 42,
      "score_members" => 42,
      "source" => "some source",
      "status" => "some status",
      "synopsis" => "some synopsis",
      "title_english" => "some title_english",
      "title_japanase" => "some title_japanase",
      "title_romaji" => "some title_romaji",
      "title_synonyms" => "some title_synonyms",
      "type" => "some type"}
  end

  test "does not create anime and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, anime_path(conn, :create), anime: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates chosen anime and renders anime when data is valid", %{conn: conn} do
    %Anime{id: id} = anime = fixture(:anime)
    conn = put conn, anime_path(conn, :update, anime), anime: @update_attrs
    assert %{"id" => ^id} = json_response(conn, 200)["data"]

    conn = get conn, anime_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "additional" => "some updated additional",
      "airing_end" => 43,
      "airing_start" => 43,
      "background" => "some updated background",
      "broadcast" => "some updated broadcast",
      "episode_duration" => "some updated episode_duration",
      "episodes" => 43,
      "favorites" => 43,
      "image" => "some updated image",
      "link_canonical" => "some updated link_canonical",
      "mal_id" => 43,
      "members" => 43,
      "popularity" => 43,
      "premiered" => "some updated premiered",
      "ranked" => 43,
      "rating" => "some updated rating",
      "score" => 43,
      "score_members" => 43,
      "source" => "some updated source",
      "status" => "some updated status",
      "synopsis" => "some updated synopsis",
      "title_english" => "some updated title_english",
      "title_japanase" => "some updated title_japanase",
      "title_romaji" => "some updated title_romaji",
      "title_synonyms" => "some updated title_synonyms",
      "type" => "some updated type"}
  end

  test "does not update chosen anime and renders errors when data is invalid", %{conn: conn} do
    anime = fixture(:anime)
    conn = put conn, anime_path(conn, :update, anime), anime: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen anime", %{conn: conn} do
    anime = fixture(:anime)
    conn = delete conn, anime_path(conn, :delete, anime)
    assert response(conn, 204)
    assert_error_sent 404, fn ->
      get conn, anime_path(conn, :show, anime)
    end
  end
end
