defmodule MalApi.Web.GenreControllerTest do
  use MalApi.Web.ConnCase

  alias MalApi.Metas
  alias MalApi.Metas.Genre

  @create_attrs %{mal_id: 42, name: "some name"}
  @update_attrs %{mal_id: 43, name: "some updated name"}
  @invalid_attrs %{mal_id: nil, name: nil}

  def fixture(:genre) do
    {:ok, genre} = Metas.create_genre(@create_attrs)
    genre
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, genre_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "creates genre and renders genre when data is valid", %{conn: conn} do
    conn = post conn, genre_path(conn, :create), genre: @create_attrs
    assert %{"id" => id} = json_response(conn, 201)["data"]

    conn = get conn, genre_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "mal_id" => 42,
      "name" => "some name"}
  end

  test "does not create genre and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, genre_path(conn, :create), genre: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates chosen genre and renders genre when data is valid", %{conn: conn} do
    %Genre{id: id} = genre = fixture(:genre)
    conn = put conn, genre_path(conn, :update, genre), genre: @update_attrs
    assert %{"id" => ^id} = json_response(conn, 200)["data"]

    conn = get conn, genre_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "mal_id" => 43,
      "name" => "some updated name"}
  end

  test "does not update chosen genre and renders errors when data is invalid", %{conn: conn} do
    genre = fixture(:genre)
    conn = put conn, genre_path(conn, :update, genre), genre: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen genre", %{conn: conn} do
    genre = fixture(:genre)
    conn = delete conn, genre_path(conn, :delete, genre)
    assert response(conn, 204)
    assert_error_sent 404, fn ->
      get conn, genre_path(conn, :show, genre)
    end
  end
end
