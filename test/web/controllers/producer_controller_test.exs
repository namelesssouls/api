defmodule MalApi.Web.ProducerControllerTest do
  use MalApi.Web.ConnCase

  alias MalApi.Metas
  alias MalApi.Metas.Producer

  @create_attrs %{mal_id: 42, name: "some name"}
  @update_attrs %{mal_id: 43, name: "some updated name"}
  @invalid_attrs %{mal_id: nil, name: nil}

  def fixture(:producer) do
    {:ok, producer} = Metas.create_producer(@create_attrs)
    producer
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, producer_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "creates producer and renders producer when data is valid", %{conn: conn} do
    conn = post conn, producer_path(conn, :create), producer: @create_attrs
    assert %{"id" => id} = json_response(conn, 201)["data"]

    conn = get conn, producer_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "mal_id" => 42,
      "name" => "some name"}
  end

  test "does not create producer and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, producer_path(conn, :create), producer: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates chosen producer and renders producer when data is valid", %{conn: conn} do
    %Producer{id: id} = producer = fixture(:producer)
    conn = put conn, producer_path(conn, :update, producer), producer: @update_attrs
    assert %{"id" => ^id} = json_response(conn, 200)["data"]

    conn = get conn, producer_path(conn, :show, id)
    assert json_response(conn, 200)["data"] == %{
      "id" => id,
      "mal_id" => 43,
      "name" => "some updated name"}
  end

  test "does not update chosen producer and renders errors when data is invalid", %{conn: conn} do
    producer = fixture(:producer)
    conn = put conn, producer_path(conn, :update, producer), producer: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen producer", %{conn: conn} do
    producer = fixture(:producer)
    conn = delete conn, producer_path(conn, :delete, producer)
    assert response(conn, 204)
    assert_error_sent 404, fn ->
      get conn, producer_path(conn, :show, producer)
    end
  end
end
