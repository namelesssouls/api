defmodule MalApi.AnimesTest do
  use MalApi.DataCase

  alias MalApi.Animes
  alias MalApi.Animes.Anime

  @create_attrs %{additional: "some additional", airing_end: 42, airing_start: 42, background: "some background", broadcast: "some broadcast", episode_duration: "some episode_duration", episodes: 42, favorites: 42, image: "some image", link_canonical: "some link_canonical", mal_id: 42, members: 42, popularity: 42, premiered: "some premiered", ranked: 42, rating: "some rating", score: 42, score_members: 42, source: "some source", status: "some status", synopsis: "some synopsis", title_english: "some title_english", title_japanase: "some title_japanase", title_romaji: "some title_romaji", title_synonyms: "some title_synonyms", type: "some type"}
  @update_attrs %{additional: "some updated additional", airing_end: 43, airing_start: 43, background: "some updated background", broadcast: "some updated broadcast", episode_duration: "some updated episode_duration", episodes: 43, favorites: 43, image: "some updated image", link_canonical: "some updated link_canonical", mal_id: 43, members: 43, popularity: 43, premiered: "some updated premiered", ranked: 43, rating: "some updated rating", score: 43, score_members: 43, source: "some updated source", status: "some updated status", synopsis: "some updated synopsis", title_english: "some updated title_english", title_japanase: "some updated title_japanase", title_romaji: "some updated title_romaji", title_synonyms: "some updated title_synonyms", type: "some updated type"}
  @invalid_attrs %{additional: nil, airing_end: nil, airing_start: nil, background: nil, broadcast: nil, episode_duration: nil, episodes: nil, favorites: nil, image: nil, link_canonical: nil, mal_id: nil, members: nil, popularity: nil, premiered: nil, ranked: nil, rating: nil, score: nil, score_members: nil, source: nil, status: nil, synopsis: nil, title_english: nil, title_japanase: nil, title_romaji: nil, title_synonyms: nil, type: nil}

  def fixture(:anime, attrs \\ @create_attrs) do
    {:ok, anime} = Animes.create_anime(attrs)
    anime
  end

  test "list_anime/1 returns all anime" do
    anime = fixture(:anime)
    assert Animes.list_anime() == [anime]
  end

  test "get_anime! returns the anime with given id" do
    anime = fixture(:anime)
    assert Animes.get_anime!(anime.id) == anime
  end

  test "create_anime/1 with valid data creates a anime" do
    assert {:ok, %Anime{} = anime} = Animes.create_anime(@create_attrs)
    assert anime.additional == "some additional"
    assert anime.airing_end == 42
    assert anime.airing_start == 42
    assert anime.background == "some background"
    assert anime.broadcast == "some broadcast"
    assert anime.episode_duration == "some episode_duration"
    assert anime.episodes == 42
    assert anime.favorites == 42
    assert anime.image == "some image"
    assert anime.link_canonical == "some link_canonical"
    assert anime.mal_id == 42
    assert anime.members == 42
    assert anime.popularity == 42
    assert anime.premiered == "some premiered"
    assert anime.ranked == 42
    assert anime.rating == "some rating"
    assert anime.score == 42
    assert anime.score_members == 42
    assert anime.source == "some source"
    assert anime.status == "some status"
    assert anime.synopsis == "some synopsis"
    assert anime.title_english == "some title_english"
    assert anime.title_japanase == "some title_japanase"
    assert anime.title_romaji == "some title_romaji"
    assert anime.title_synonyms == "some title_synonyms"
    assert anime.type == "some type"
  end

  test "create_anime/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Animes.create_anime(@invalid_attrs)
  end

  test "update_anime/2 with valid data updates the anime" do
    anime = fixture(:anime)
    assert {:ok, anime} = Animes.update_anime(anime, @update_attrs)
    assert %Anime{} = anime
    assert anime.additional == "some updated additional"
    assert anime.airing_end == 43
    assert anime.airing_start == 43
    assert anime.background == "some updated background"
    assert anime.broadcast == "some updated broadcast"
    assert anime.episode_duration == "some updated episode_duration"
    assert anime.episodes == 43
    assert anime.favorites == 43
    assert anime.image == "some updated image"
    assert anime.link_canonical == "some updated link_canonical"
    assert anime.mal_id == 43
    assert anime.members == 43
    assert anime.popularity == 43
    assert anime.premiered == "some updated premiered"
    assert anime.ranked == 43
    assert anime.rating == "some updated rating"
    assert anime.score == 43
    assert anime.score_members == 43
    assert anime.source == "some updated source"
    assert anime.status == "some updated status"
    assert anime.synopsis == "some updated synopsis"
    assert anime.title_english == "some updated title_english"
    assert anime.title_japanase == "some updated title_japanase"
    assert anime.title_romaji == "some updated title_romaji"
    assert anime.title_synonyms == "some updated title_synonyms"
    assert anime.type == "some updated type"
  end

  test "update_anime/2 with invalid data returns error changeset" do
    anime = fixture(:anime)
    assert {:error, %Ecto.Changeset{}} = Animes.update_anime(anime, @invalid_attrs)
    assert anime == Animes.get_anime!(anime.id)
  end

  test "delete_anime/1 deletes the anime" do
    anime = fixture(:anime)
    assert {:ok, %Anime{}} = Animes.delete_anime(anime)
    assert_raise Ecto.NoResultsError, fn -> Animes.get_anime!(anime.id) end
  end

  test "change_anime/1 returns a anime changeset" do
    anime = fixture(:anime)
    assert %Ecto.Changeset{} = Animes.change_anime(anime)
  end
end
