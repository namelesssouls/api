defmodule MalApi.Repo.Migrations.CreateMalApi.Entity.Character do
  use Ecto.Migration

  def change do
    create table(:entity_characters, primary_key: false) do
      add :mal_id, :integer, primary_key: true
      add :image, :string
      add :url, :string
      add :name, :string
      add :role, :string

      timestamps()
    end

    create unique_index(:entity_characters, [:mal_id])
  end
end
