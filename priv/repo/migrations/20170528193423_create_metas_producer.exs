defmodule MalApi.Repo.Migrations.CreateMalApi.Metas.Producer do
  use Ecto.Migration

  def change do
    create table(:metas_producer, primary_key: false) do
      add :mal_id, :integer, primary_key: true
      add :name, :string

      timestamps()
    end

    create unique_index(:metas_producer, [:mal_id])
  end
end
