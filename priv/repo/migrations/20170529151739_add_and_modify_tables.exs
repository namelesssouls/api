defmodule MalApi.Repo.Migrations.AddAndModifyTables do
  use Ecto.Migration

  def change do
    rename table(:animes_anime), :broadcast, to: :broadcast_day
    alter table(:animes_anime) do
      add :broadcast_time, :text
      modify :airing_end, :text
      modify :airing_start, :text
    end
  end
end
