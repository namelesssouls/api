defmodule MalApi.Repo.Migrations.CreateMalApi.Entity.People do
  use Ecto.Migration

  def change do
    create table(:entity_people, primary_key: false) do
      add :mal_id, :integer, primary_key: true
      add :name, :string
      add :url, :string
      add :image, :string

      timestamps()
    end

    create unique_index(:entity_people, [:mal_id])
  end
end
