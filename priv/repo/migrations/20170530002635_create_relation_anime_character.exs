defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeCharacter do
  use Ecto.Migration

  def change do
    create table(:anime_characters, primary_key: false) do
      add :anime_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)
      add :character_id, references(:entity_characters, on_delete: :delete_all, column: :mal_id)

      timestamps()
    end

    create index(:anime_characters, [:anime_id])
    create index(:anime_characters, [:character_id])
  end
end
