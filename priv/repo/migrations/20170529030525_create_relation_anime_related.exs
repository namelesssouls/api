defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeRelated do
  use Ecto.Migration

  def change do
    create table(:anime_related) do
      add :name, :string
      add :url, :string
      add :type, :string
      add :anime_mal_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)

      timestamps()
    end

    create index(:anime_related, [:anime_mal_id])
  end
end
