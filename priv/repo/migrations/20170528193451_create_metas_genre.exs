defmodule MalApi.Repo.Migrations.CreateMalApi.Metas.Genre do
  use Ecto.Migration

  def change do
    create table(:metas_genre, primary_key: false) do
      add :mal_id, :integer, primary_key: true
      add :name, :string

      timestamps()
    end

    create unique_index(:metas_genre, [:mal_id])
  end
end
