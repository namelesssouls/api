defmodule MalApi.Repo.Migrations.CreateMalApi.Animes.Anime do
  use Ecto.Migration

  def change do
    create table(:animes_anime, primary_key: false) do
      add :mal_id, :integer, primary_key: true
      add :link_canonical, :string
      add :image, :string
      add :title_romaji, :string
      add :title_synonyms, :string
      add :title_english, :string
      add :title_japanase, :string
      add :type, :string
      add :episodes, :integer
      add :rating, :string
      add :status, :string
      add :airing_start, :integer
      add :airing_end, :integer
      add :synopsis, :string
      add :background, :string
      add :additional, :string
      add :episode_duration, :string
      add :source, :string
      add :broadcast, :string
      add :premiered, :string
      add :score, :float
      add :score_members, :integer
      add :ranked, :integer
      add :popularity, :integer
      add :members, :integer
      add :favorites, :integer

      timestamps()
    end

    create unique_index(:animes_anime, [:mal_id])
  end
end
