defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeGenre do
  use Ecto.Migration

  def change do
    create table(:anime_genres, primary_key: false) do
      add :anime_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)
      add :genre_id, references(:metas_genre, on_delete: :delete_all, column: :mal_id)
    end

    create index(:anime_genres, [:anime_id])
    create index(:anime_genres, [:genre_id])
  end
end
