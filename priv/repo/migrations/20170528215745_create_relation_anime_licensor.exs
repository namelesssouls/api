defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeLicensor do
  use Ecto.Migration

  def change do
    create table(:anime_licensors, primary_key: false) do
      add :anime_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)
      add :producer_id, references(:metas_producer, on_delete: :delete_all, column: :mal_id)
    end

    create index(:anime_licensors, [:anime_id])
    create index(:anime_licensors, [:producer_id])
  end
end
