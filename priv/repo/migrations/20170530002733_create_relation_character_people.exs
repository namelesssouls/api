defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.CharacterPeople do
  use Ecto.Migration

  def change do
    create table(:character_people, primary_key: false) do
      add :country, :string
      add :character_id, references(:entity_characters, on_delete: :delete_all, column: :mal_id)
      add :people_id, references(:entity_people, on_delete: :delete_all, column: :mal_id)

      timestamps()
    end

    create index(:character_people, [:character_id])
    create index(:character_people, [:people_id])
  end
end
