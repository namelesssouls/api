defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeStudio do
  use Ecto.Migration

  def change do
    create table(:anime_studios, primary_key: false) do
      add :anime_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)
      add :producer_id, references(:metas_producer, on_delete: :delete_all, column: :mal_id)
    end

    create index(:anime_studios, [:anime_id])
    create index(:anime_studios, [:producer_id])
  end
end
