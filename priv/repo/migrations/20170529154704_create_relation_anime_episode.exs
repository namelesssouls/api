defmodule MalApi.Repo.Migrations.CreateMalApi.Relation.AnimeEpisode do
  use Ecto.Migration

  def change do
    create table(:anime_episodes) do
      add :number, :integer
      add :title, :string
      add :title_japanese, :string
      add :title_romaji, :string
      add :aired, :string
      add :caption, :string
      add :video_url, :string
      add :forum_url, :string
      add :anime_mal_id, references(:animes_anime, on_delete: :delete_all, column: :mal_id)

      timestamps()
    end

    create index(:anime_episodes, [:anime_mal_id])
  end
end
