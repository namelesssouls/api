defmodule MalApi.Repo.Migrations.AddUrlToTables do
  use Ecto.Migration

  def change do
    alter table(:metas_genre) do
      add :url, :string
    end
    alter table(:metas_producer) do
      add :url, :string
    end
  end
end
