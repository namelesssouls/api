defmodule MalApi.Repo.Migrations.AddTypeIdToRelated do
  use Ecto.Migration

  def change do
    alter table(:anime_related) do
      add :type_id, :int
    end
  end
end
