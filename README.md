# MalApi

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

# anime table migration
mix phx.gen.json Animes Anime anime mal_id:integer:unique link_canonical:string image:string title_romaji:string title_synonyms:string title_english:string title_japanase:string type:string episodes:integer rating:string status:string airing_start:integer airing_end:integer synopsis:string background:string additional:string episode_duration:string source:string broadcast:string premiered:string score:integer score_members:integer ranked:integer popularity:integer members:integer favorites:integer

# manga table migration
mix phx.gen.schema Manga.Manga manga_mangas mal_id:integer:unique link_canonical:string synopsis:string title:string image:string japanese:string english:string volumes:integer chapters:integer status:string published:string ranked:integer popularity:integer members:integer

# producers table mix
mix phx.gen.json Metas Producer producer mal_id:integer:unique name:string

# genres table mix
mix phx.gen.json Metas Genre genre mal_id:integer:unique name:string

# Genre relationship
mix phx.gen.schema Relation.AnimeGenre anime_genres anime_id:references:animes_anime genre_id:references:metas_genre

# Producer relationship
mix phx.gen.schema Relation.AnimeProducer anime_producers anime_producer:references:animes_anime producer_id:references:metas_producer

# Licensor relationship
mix phx.gen.schema Relation.AnimeLicensor anime_licensors anime_producer:references:animes_anime producer_id:references:metas_producer

# Studio relationship
mix phx.gen.schema Relation.AnimeStudio anime_studios anime_producer:references:animes_anime producer_id:references:metas_producer

# Related relationship
mix phx.gen.schema Relation.AnimeRelated anime_related name:string url:string type:string anime_mal_id:references:animes_anime

# Episodes relationship
mix phx.gen.schema Relation.AnimeEpisode anime_episodes number:integer title:string title-japanese:string title-romanji:string aired:integer caption:string video_url:string forum_url:string anime_mal_id:references:animes_anime

# Characters table
mix phx.gen.schema Entity.Character entity_characters mal_id:integer:unique image:string url:string name:string role:string

# Characters-anime Relationship
mix phx.gen.schema Relation.AnimeCharacter anime_characters anime_id:references:animes_anime character_id:references:entity_characters

# People table
mix phx.gen.schema Entity.People entity_people mal_id:integer:unique name:string

# Character-actor relationship
mix phx.gen.schema Relation.CharacterPeople character_people country:string character_id:references:entity_characters people_id:references:entity_people
