defmodule MalApi.Entity.People do
  use Ecto.Schema

  @primary_key {:mal_id, :integer, []}
  @derive {Phoenix.Param, key: :mal_id}
  schema "entity_people" do
    field :name, :string
    field :url, :string
    field :image, :string

    timestamps()
  end
end
