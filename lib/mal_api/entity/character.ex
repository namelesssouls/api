defmodule MalApi.Entity.Character do
  use Ecto.Schema

  @primary_key {:mal_id, :integer, []}
  @derive {Phoenix.Param, key: :mal_id}
  schema "entity_characters" do
    field :image, :string
    field :name, :string
    field :role, :string
    field :url, :string

    has_many :actors, MalApi.Relation.CharacterPeople, foreign_key: :character_id

    timestamps()
  end
end
