defmodule MalApi.Animes do
  @moduledoc """
  The boundary for the Animes system.
  """

  import Ecto.{Query, Changeset}, warn: false
  alias MalApi.Repo

  alias MalApi.Animes.Anime
  alias MalApi.Entity.Character


  @doc """
  Gets a single anime.

  Raises {:error, "Anime id (id) not found"} if the Anime does not exist.

  """
  def get_anime(%{id: id}, _info) do
    case Repo.get(Anime, id) do
      nil  -> {:error, "Anime id #{id} not found"}
      anime -> {:ok, anime}
    end
  end

  @doc """
  Get a single character

  Raises {:error, "Character id (id) not found"} if the Anime does not exist.

  """
  def get_character(%{id: id}, _info) do
    case Repo.get(Character, id) do
      nil  -> {:error, "Character id #{id} not found"}
      character -> {:ok, character}
    end
  end
end
