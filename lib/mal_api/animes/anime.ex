defmodule MalApi.Animes.Anime do
  use Ecto.Schema

  @primary_key {:mal_id, :integer, []}
  @derive {Phoenix.Param, key: :mal_id}
  schema "animes_anime" do
    field :additional, :string
    field :airing_end, :string
    field :airing_start, :string
    field :background, :string
    field :broadcast_day, :string
    field :broadcast_time, :string
    field :episode_duration, :string
    field :episodes, :integer
    field :favorites, :integer
    field :image, :string
    field :link_canonical, :string
    field :members, :integer
    field :popularity, :integer
    field :premiered, :string
    field :ranked, :integer
    field :rating, :string
    field :score, :float
    field :score_members, :integer
    field :source, :string
    field :status, :string
    field :synopsis, :string
    field :title_english, :string
    field :title_japanase, :string
    field :title_romaji, :string
    field :title_synonyms, :string
    field :type, :string

    many_to_many :genres, MalApi.Metas.Genre, join_through: "anime_genres", join_keys: [anime_id: :mal_id, genre_id: :mal_id]
    many_to_many :licensors, MalApi.Metas.Producer, join_through: "anime_licensors", join_keys: [anime_id: :mal_id, producer_id: :mal_id]
    many_to_many :producers, MalApi.Metas.Producer, join_through: "anime_producers", join_keys: [anime_id: :mal_id, producer_id: :mal_id]
    many_to_many :studios, MalApi.Metas.Producer, join_through: "anime_studios", join_keys: [anime_id: :mal_id, producer_id: :mal_id]
    many_to_many :characters, MalApi.Entity.Character, join_through: "anime_characters", join_keys: [anime_id: :mal_id, character_id: :mal_id]
    has_many :related, MalApi.Relation.AnimeRelated
    has_many :episodes_list, MalApi.Relation.AnimeEpisode

    timestamps()
  end
end
