defmodule MalApi.Relation.AnimeCharacter do
  use Ecto.Schema

  @primary_key false
  schema "anime_characters" do
    field :anime_id, :id
    field :character_id, :id

    timestamps()
  end
end
