defmodule MalApi.Relation.AnimeGenre do
  use Ecto.Schema

  @primary_key false
  schema "anime_genres" do
    field :anime_id, :id
    field :genre_id, :id
  end
end
