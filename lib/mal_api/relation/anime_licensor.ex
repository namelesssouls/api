defmodule MalApi.Relation.AnimeLicensor do
  use Ecto.Schema

  @primary_key false
  schema "anime_licensors" do
    field :anime_id, :id
    field :producer_id, :id
  end
end
