defmodule MalApi.Relation.AnimeStudio do
  use Ecto.Schema

  @primary_key false
  schema "anime_studios" do
    field :anime_id, :id
    field :producer_id, :id
  end
end
