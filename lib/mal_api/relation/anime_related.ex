defmodule MalApi.Relation.AnimeRelated do
  use Ecto.Schema

  schema "anime_related" do
    field :name, :string
    field :type, :string
    field :url, :string
    field :type_id, :integer
    field :anime_mal_id, :id

    timestamps()
  end
end
