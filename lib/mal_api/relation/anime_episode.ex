defmodule MalApi.Relation.AnimeEpisode do
  use Ecto.Schema

  schema "anime_episodes" do
    field :aired, :string
    field :caption, :string
    field :forum_url, :string
    field :number, :integer
    field :title, :string
    field :title_japanese, :string
    field :title_romaji, :string
    field :video_url, :string
    field :anime_mal_id, :id

    timestamps()
  end
end
