defmodule MalApi.Relation.AnimeProducer do
  use Ecto.Schema

  @primary_key false
  schema "anime_producers" do
    field :anime_id, :id
    field :producer_id, :id
  end
end
