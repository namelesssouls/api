defmodule MalApi.Relation.CharacterPeople do
  use Ecto.Schema

  @primary_key false
  schema "character_people" do
    field :country, :string
    belongs_to :entity_character, MalApi.Entity.Character, foreign_key: :character_id, references: :mal_id
    belongs_to :entity_people, MalApi.Entity.People, foreign_key: :people_id, references: :mal_id

    timestamps()
  end
end
