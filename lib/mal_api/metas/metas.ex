defmodule MalApi.Metas do
  @moduledoc """
  The boundary for the Metas system.
  """

  import Ecto.{Query, Changeset}, warn: false
  alias MalApi.Repo

  alias MalApi.Metas.Producer

  @doc """
  Returns the list of producer.

  ## Examples

      iex> list_producer()
      [%Producer{}, ...]

  """
  def list_producer do
    Repo.all(Producer)
  end

  @doc """
  Gets a single producer.

  Raises `Ecto.NoResultsError` if the Producer does not exist.

  ## Examples

      iex> get_producer!(123)
      %Producer{}

      iex> get_producer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_producer!(id), do: Repo.get!(Producer, id)

  @doc """
  Creates a producer.

  ## Examples

      iex> create_producer(%{field: value})
      {:ok, %Producer{}}

      iex> create_producer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_producer(attrs \\ %{}) do
    %Producer{}
    |> producer_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a producer.

  ## Examples

      iex> update_producer(producer, %{field: new_value})
      {:ok, %Producer{}}

      iex> update_producer(producer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_producer(%Producer{} = producer, attrs) do
    producer
    |> producer_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Producer.

  ## Examples

      iex> delete_producer(producer)
      {:ok, %Producer{}}

      iex> delete_producer(producer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_producer(%Producer{} = producer) do
    Repo.delete(producer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking producer changes.

  ## Examples

      iex> change_producer(producer)
      %Ecto.Changeset{source: %Producer{}}

  """
  def change_producer(%Producer{} = producer) do
    producer_changeset(producer, %{})
  end

  defp producer_changeset(%Producer{} = producer, attrs) do
    producer
    |> cast(attrs, [:mal_id, :name])
    |> validate_required([:mal_id, :name])
    |> unique_constraint(:mal_id)
  end

  alias MalApi.Metas.Genre

  @doc """
  Returns the list of genre.

  ## Examples

      iex> list_genre()
      [%Genre{}, ...]

  """
  def list_genre do
    Repo.all(Genre)
  end

  @doc """
  Gets a single genre.

  Raises `Ecto.NoResultsError` if the Genre does not exist.

  ## Examples

      iex> get_genre!(123)
      %Genre{}

      iex> get_genre!(456)
      ** (Ecto.NoResultsError)

  """
  def get_genre!(id), do: Repo.get!(Genre, id)

  @doc """
  Creates a genre.

  ## Examples

      iex> create_genre(%{field: value})
      {:ok, %Genre{}}

      iex> create_genre(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_genre(attrs \\ %{}) do
    %Genre{}
    |> genre_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a genre.

  ## Examples

      iex> update_genre(genre, %{field: new_value})
      {:ok, %Genre{}}

      iex> update_genre(genre, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_genre(%Genre{} = genre, attrs) do
    genre
    |> genre_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Genre.

  ## Examples

      iex> delete_genre(genre)
      {:ok, %Genre{}}

      iex> delete_genre(genre)
      {:error, %Ecto.Changeset{}}

  """
  def delete_genre(%Genre{} = genre) do
    Repo.delete(genre)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking genre changes.

  ## Examples

      iex> change_genre(genre)
      %Ecto.Changeset{source: %Genre{}}

  """
  def change_genre(%Genre{} = genre) do
    genre_changeset(genre, %{})
  end

  defp genre_changeset(%Genre{} = genre, attrs) do
    genre
    |> cast(attrs, [:mal_id, :name])
    |> validate_required([:mal_id, :name])
    |> unique_constraint(:mal_id)
  end
end
