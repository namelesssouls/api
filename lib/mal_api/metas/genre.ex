defmodule MalApi.Metas.Genre do
  use Ecto.Schema

  @primary_key {:mal_id, :integer, []}
  @derive {Phoenix.Param, key: :mal_id}
  schema "metas_genre" do
    field :name, :string
    field :url, :string

    many_to_many :animes, Anime, join_through: "anime_genres", join_keys: [anime_id: :mal_id, genre_id: :mal_id]

    timestamps()
  end
end
