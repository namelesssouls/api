defmodule MalApi.Metas.Producer do
  use Ecto.Schema

  @primary_key {:mal_id, :integer, []}
  @derive {Phoenix.Param, key: :mal_id}
  schema "metas_producer" do
    field :name, :string
    field :url, :string
    
    timestamps()
  end
end
