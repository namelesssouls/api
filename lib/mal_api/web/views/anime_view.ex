defmodule MalApi.Web.AnimeView do
  use MalApi.Web, :view
  # alias MalApi.Web.AnimeView

  # def render("index.json", %{anime: anime}) do
  #   %{data: render_many(anime, AnimeView, "anime.json")}
  # end

  def render("show.json", %{anime: anime}) do
    anime
  end

  # def render("anime.json", %{anime: anime}) do
  #   %{mal_id: anime.mal_id,
  #     link_canonical: anime.link_canonical,
  #     image: anime.image,
  #     title_romaji: anime.title_romaji,
  #     title_synonyms: anime.title_synonyms,
  #     title_english: anime.title_english,
  #     title_japanase: anime.title_japanase,
  #     type: anime.type,
  #     episodes: anime.episodes,
  #     rating: anime.rating,
  #     status: anime.status,
  #     airing_start: anime.airing_start,
  #     airing_end: anime.airing_end,
  #     synopsis: anime.synopsis,
  #     background: anime.background,
  #     additional: anime.additional,
  #     episode_duration: anime.episode_duration,
  #     source: anime.source,
  #     broadcast: anime.broadcast,
  #     premiered: anime.premiered,
  #     score: anime.score,
  #     score_members: anime.score_members,
  #     ranked: anime.ranked,
  #     popularity: anime.popularity,
  #     members: anime.members,
  #     favorites: anime.favorites,
  #     genres: map_array(anime.genres),
  #     licensors: map_array(anime.licensors),
  #     producers: map_array(anime.producers),
  #     studios: map_array(anime.studios)}
  # end

  # defp map_array(data) do
  #   Enum.map(data, fn (x) -> %{name: x.name, id: x.mal_id} end)
  # end
end
