defmodule MalApi.Web.Schema.Types do
  use Absinthe.Schema.Notation
  import_types Absinthe.Type.Custom
  use Absinthe.Ecto, repo: MalApi.Repo

  @desc "Describes all MAL Anime information"
  object :anime do
    @desc "Anime Mal identifier"
    field :mal_id, :id
    @desc "Anime additional information"
    field :additional, :string
    @desc "The end date of airing (same as described in MAL)"
    field :airing_end, :string
    @desc "The starting date of airing"
    field :airing_start, :string
    @desc "Anime background information"
    field :background, :string
    @desc "The day when the anime (movie) was broadcasted or is broadcast (ariring anime TV)"
    field :broadcast_day, :string
    @desc "The day time when is broadcast"
    field :broadcast_time, :string
    @desc "The normal episode duration"
    field :episode_duration, :string
    @desc "The total quantity of episodes actually"
    field :episodes, :integer
    @desc "How much people has this anime in their favorites"
    field :favorites, :integer
    @desc "Mal image url"
    field :image, :string
    @desc "MAL canonical url"
    field :link_canonical, :string
    @desc "The quantity of members in this animes"
    field :members, :integer
    @desc "The anime popularity"
    field :popularity, :integer
    @desc "The season when was premiered"
    field :premiered, :string
    @desc "MAL rank number"
    field :ranked, :integer
    @desc "The anime audence rating"
    field :rating, :string
    @desc "Anime score by the community"
    field :score, :float
    @desc "The quantity of member who was liked the anime"
    field :score_members, :integer
    @desc "The anime source (Manga, Web comic etc...)"
    field :source, :string
    @desc "The actual anime status (Airing, Finished etc...)"
    field :status, :string
    @desc "Anime synopsis"
    field :synopsis, :string
    @desc "Anime english title equivalent"
    field :title_english, :string
    @desc "The day when the anime (movie) was broadcasted or is broadcast (ariring anime TV)"
    field :title_japanase, :string
    @desc "Title in romaji equivalent (normally the official MAL anime title)"
    field :title_romaji, :string
    @desc "The anime name synonyms"
    field :title_synonyms, :string
    @desc "Anime type (TV, Movie, Ova etc..."
    field :type, :string
    @desc "The last date where was updated this record"
    field :updated_at, :naive_datetime

    @desc "List of genres in the anime"
    field :genres, list_of(:genre), resolve: assoc(:genres)
    @desc "List of licensors in the anime"
    field :licensors, list_of(:licensor), resolve: assoc(:licensors)
    @desc "List of producers in the anime"
    field :producers, list_of(:producer), resolve: assoc(:producers)
    @desc "List of studios in the anime"
    field :studios, list_of(:studio), resolve: assoc(:studios)
    @desc "List of related in the anime"
    field :related, list_of(:relate), resolve: assoc(:related)
    @desc "Episodes list of the anime"
    field :episodes_list, list_of(:episode), resolve: assoc(:episodes_list)
    @desc "List of characters in the anime"
    field :characters, list_of(:character), resolve: assoc(:characters)
  end

  @desc "MAL Character Entity"
  object :character do
    @desc "Character Image MAL url"
    field :image, :string
    @desc "Character Romaji/Oficial name"
    field :name, :string
    @desc "Describes if the character is main or support"
    field :role, :string
    @desc "Character MAL canonical url"
    field :url, :string
    @desc "Last record update"
    field :updated_at, :naive_datetime

    @desc "A list of all voice actors of this character"
    field :actors, list_of(:character_people), resolve: assoc(:actors)
  end

  object :character_people do
    @desc "The country (language) of the voice actor"
    field :country, :string
    @desc "The voice Actor information"
    field :actor, :people, resolve: assoc(:entity_people)
  end

  @desc "Describes a person can be voice actor, director etc..."
  object :people do
    @desc "People name (if is japanese people return the romaji equivalent)"
    field :name, :string
    @desc "People MAL canonical url"
    field :url, :string
    @desc "People MAL Image"
    field :image, :string
  end

  @desc "Describes all data from a anime episode"
  object :episode do
    @desc "Database ID"
    field :id, :id
    @desc "Date of anime airing (same as show in MAL website)"
    field :aired, :string
    @desc "Caption field, it shows info from the chapter like \"filler\", \"recap\" etc..."
    field :caption, :string
    @desc "MAL Forum episode link"
    field :forum_url, :string
    @desc "The number of episode (order)"
    field :number, :integer
    @desc "The episode english name"
    field :title, :string
    @desc "Japanese name version"
    field :title_japanese, :string
    @desc "Romaji of Japanese version"
    field :title_romaji, :string
    @desc "Episode legal video streaming"
    field :video_url, :string
  end

  @desc "Describes what kind of genre is the theme (anime/manga)"
  object :genre do
    @desc "People name (if is japanese people return the romaji equivalent)"
    field :mal_id, :id
    @desc "Genre name"
    field :name, :string
    @desc "Anime Genre MAL url"
    field :url, :string
  end

  @desc "Anime Related data (Adaptations, Sequel etc...)"
  object :relate do
    @desc "Database ID"
    field :id, :id
    @desc "Related name"
    field :name, :string
    @desc "Anime/Manga MAL Url"
    field :url, :string
    @desc "Type of related (Adaptation, Sequel etc...)"
    field :type, :string
    @desc "Anime/Manga id"
    field :type_id, :integer
  end

  @desc "Anime licensors info (all them are producers)"
  object :licensor do
    @desc "Licensor MAL identifier"
    field :mal_id, :id
    @desc "Licensor official name"
    field :name, :string
    @desc "Licensor MAL url"
    field :url, :string
  end

  @desc "Producer entity (bussines, studio etc...)"
  object :producer do
    @desc "Producer MAL identifier"
    field :mal_id, :id
    @desc "Producer name"
    field :name, :string
    @desc "Producer MAL url"
    field :url, :string
  end

  @desc "Studio Info (all them are producers)"
  object :studio do
    @desc "Studio MAL identifier"
    field :mal_id, :id
    @desc "Studio name"
    field :name, :string
    @desc "Studio MAL url"
    field :url, :string
  end
end