defmodule MalApi.Web.Schema do
  use Absinthe.Schema
  import_types MalApi.Web.Schema.Types

  query do
    @desc "Get a single MAL Anime information with their respective relations [Request by ID]"
    field :anime, type: :anime do
      arg :id, non_null(:id)
      resolve &MalApi.Animes.get_anime/2
    end

    @desc "Get a single MAL Character information and his voice actors [Request by ID]"
    field :character, type: :character do
      arg :id, non_null(:id)
      resolve &MalApi.Animes.get_character/2
    end
  end

end