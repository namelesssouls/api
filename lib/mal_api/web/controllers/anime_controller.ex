defmodule MalApi.Web.AnimeController do
  use MalApi.Web, :controller

  action_fallback MalApi.Web.FallbackController

  def characters(conn, %{"id" => id}) do
    doc = """
    {
      anime(id: #{id}) {
        additional,
        airingEnd,
        airingStart,
        background,
        broadcastDay,
        broadcastTime,
        episodeDuration,
        episodes,
        favorites,
        image,
        linkCanonical,
        malId,
        members,
        popularity,
        premiered,
        ranked,
        rating,
        score,
        scoreMembers,
        source,
        status,
        synopsis,
        titleEnglish,
        titleJapanase,
        titleRomaji,
        titleSynonyms,
        type,
        genres {
          malId
          name
          url
        },
        licensors{
          malId,
          url,
          name
        },
        producers {
          malId,
          url,
          name
        },
        studios {
          malId
          name
          url
        }
        characters {
          name,
          image,
          role,
          url,
          actors {
            country,
            actor {
              name,
              url,
              image
            }
          }
        }
      }
    }
    """
    case Absinthe.run(doc, MalApi.Web.Schema) do
      {:error, data} -> render(conn, "show.json", anime: data)
      {:ok, %{:errors => _} = data} -> render(conn, "show.json", anime: data)
      {:ok, %{:data => data}} -> render(conn, "show.json", anime: data)
    end
  end

  def episodes(conn, %{"id" => id}) do
    doc = """
    {
      anime(id: #{id}) {
        additional,
        airingEnd,
        airingStart,
        background,
        broadcastDay,
        broadcastTime,
        episodeDuration,
        episodes,
        favorites,
        image,
        linkCanonical,
        malId,
        members,
        popularity,
        premiered,
        ranked,
        rating,
        score,
        scoreMembers,
        source,
        status,
        synopsis,
        titleEnglish,
        titleJapanase,
        titleRomaji,
        titleSynonyms,
        type,
        genres {
          malId
          name
          url
        },
        licensors{
          malId,
          url,
          name
        },
        producers {
          malId,
          url,
          name
        },
        studios {
          malId
          name
          url
        }
        episodesList {
          aired
          caption
          forumUrl
          number
          title
          titleJapanese
          titleRomaji
          videoUrl
        }
      }
    }
    """
    case Absinthe.run(doc, MalApi.Web.Schema) do
      {:error, data} -> render(conn, "show.json", anime: data)
      {:ok, %{:errors => _} = data} -> render(conn, "show.json", anime: data)
      {:ok, %{:data => data}} -> render(conn, "show.json", anime: data)
    end
  end

  def show(conn, %{"id" => id}) do
    doc = """
    {
      anime(id: #{id}) {
        additional,
        airingEnd,
        airingStart,
        background,
        broadcastDay,
        broadcastTime,
        episodeDuration,
        episodes,
        favorites,
        image,
        linkCanonical,
        malId,
        members,
        popularity,
        premiered,
        ranked,
        rating,
        score,
        scoreMembers,
        source,
        status,
        synopsis,
        titleEnglish,
        titleJapanase,
        titleRomaji,
        titleSynonyms,
        type,
        genres {
          malId
          name
          url
        },
        licensors{
          malId,
          url,
          name
        },
        producers {
          malId,
          url,
          name
        }
        studios {
          malId
          name
          url
        }
      }
    }
    """
    case Absinthe.run(doc, MalApi.Web.Schema) do
      {:error, data} -> render(conn, "show.json", anime: data)
      {:ok, %{:errors => _} = data} -> render(conn, "show.json", anime: data)
      {:ok, %{:data => data}} -> render(conn, "show.json", anime: data)
    end
  end

  def all(conn, %{"id" => id}) do
    doc = """
    {
      anime(id: #{id}) {
        additional,
        airingEnd,
        airingStart,
        background,
        broadcastDay,
        broadcastTime,
        episodeDuration,
        episodes,
        favorites,
        image,
        linkCanonical,
        malId,
        members,
        popularity,
        premiered,
        ranked,
        rating,
        score,
        scoreMembers,
        source,
        status,
        synopsis,
        titleEnglish,
        titleJapanase,
        titleRomaji,
        titleSynonyms,
        type,
        genres {
          malId
          name
          url
        },
        licensors{
          malId,
          url,
          name
        },
        producers {
          malId,
          url,
          name
        },
        studios {
          malId
          name
          url
        }
        episodesList {
          aired
          caption
          forumUrl
          number
          title
          titleJapanese
          titleRomaji
          videoUrl
        },
        characters {
          name,
          image,
          role,
          url,
          actors {
            country,
            actor {
              name,
              url,
              image
            }
          }
        }
      }
    }
    """
    case Absinthe.run(doc, MalApi.Web.Schema) do
      {:error, data} -> render(conn, "show.json", anime: data)
      {:ok, %{:errors => _} = data} -> render(conn, "show.json", anime: data)
      {:ok, %{:data => data}} -> render(conn, "show.json", anime: data)
    end
  end
end
