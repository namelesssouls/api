defmodule MalApi.Web.Router do
  use MalApi.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MalApi.Web do
    pipe_through :api

    get "/anime/:id/characters", AnimeController, :characters
    get "/anime/:id/episodes", AnimeController, :episodes
    get "/anime/:id/all", AnimeController, :all
    get "/anime/:id", AnimeController, :show
  end

  forward "/graph", Absinthe.Plug,
    schema: MalApi.Web.Schema
  
  forward "/graphiql", Absinthe.Plug.GraphiQL,
    schema: MalApi.Web.Schema
end
